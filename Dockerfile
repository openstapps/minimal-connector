FROM registry.gitlab.com/openstapps/projectmanagement/node

WORKDIR /minimal-connector
ENTRYPOINT ["node", "lib/cli.js", "run", "--"]
CMD [""]

# Add connector as last step to reuse layers effectively
ADD . /minimal-connector