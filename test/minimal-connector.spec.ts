/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThings, SCThingType} from '@openstapps/core';
import {Validator} from '@openstapps/core-tools/lib/validate';
import {expect} from 'chai';
import {suite, test} from '@testdeck/mocha';
import {join} from 'path';
import {MinimalConnector} from '../src/minimal-connector';

@suite
export class MinimalConnectorSpec {
  private static connector: MinimalConnector;
  private static validator: Validator;

  static async before() {
    this.validator = new Validator();
    await this.validator.addSchemas(
      join(__dirname, '..', 'node_modules', '@openstapps', 'core', 'lib', 'schema'),
    );
    this.connector = new MinimalConnector('f-u', 'minimal-connector');
  }

  /**
   * Gets the `SCThingType`-key as string
   *
   * @param instance Contains `type` with the value of a SCThingType-key
   */
  static getSchemaNameFromType<T extends SCThings>(instance: T): string {
    const type = instance.type;
    const index = Object.values(SCThingType).indexOf(type);
    const key = Object.keys(SCThingType)[index];
    return `SC${key}`;
  }

  /**
   * Checks, if the items are valid
   *
   * @param things Items fetched by the connector
   */
  static validateThings<T extends SCThings>(things: T[]) {
    things.forEach((thing: T) => {
      const schemaName = this.getSchemaNameFromType<T>(thing);
      // validate thing
      const validatorResult = MinimalConnectorSpec.validator.validate(thing, schemaName);
      expect(validatorResult.errors).to.have.lengthOf(
        0,
        JSON.stringify({
          errors: validatorResult.errors,
          thing: thing,
        }),
      );
    });
  }

  @test
  getSampleThings() {
    return MinimalConnectorSpec.connector
      .getItems()
      .then(<T extends SCThings>(items: T[]) => {
        if (items.length > 0) {
          MinimalConnectorSpec.validateThings(items);
        }
      });
  }
}
