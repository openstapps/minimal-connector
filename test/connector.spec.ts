/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingOriginType} from '@openstapps/core';
import {expect} from 'chai';
import {suite, test} from '@testdeck/mocha';
import {MinimalConnector} from '../src/minimal-connector';

@suite
export class ConnectorSpec {
  private static connector: MinimalConnector;

  static async before() {
    this.connector = new MinimalConnector('f-u', 'minimal-connector');
  }

  @test
  testCreateRemoteOrigin() {
    const remoteOrigin = ConnectorSpec.connector.createRemoteOrigin();
    expect(remoteOrigin.name).to.equal(ConnectorSpec.connector.origin);
    expect(remoteOrigin.type).to.equal(SCThingOriginType.Remote);
    expect(new Date().valueOf()).to.be.at.least(Date.parse(remoteOrigin.indexed).valueOf());
  }

  @test
  async testAutomaticMissingUIDGeneration() {
    const uuidRegExp = new RegExp('^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$');
    const messages = await ConnectorSpec.connector.getItems();
    for(const message of messages){
      expect(message.uid).to.match(uuidRegExp);
    }
  }

}
