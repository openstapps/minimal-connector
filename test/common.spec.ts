/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingType, SCBulkResponse, SCLicensePlate} from '@openstapps/core';
import {expect} from 'chai';
import {suite, test} from '@testdeck/mocha';
import {MinimalConnector} from '../src/minimal-connector';
import {createUUID, executeConnector, isValidSCNamespace} from '../src/common';
import nock = require('nock');

@suite
export class CommonSpec {
  private static connector: MinimalConnector;

  static async before() {
    this.connector = new MinimalConnector('f-u', 'minimal-connector');
  }

  @test
  public isValidSCNamespace() {
    const existingAddedSCLIcensePlate: SCLicensePlate = 'f-u';
    const notASCLicensePlate: string = 'NOT-A-LICENSE';
    const existingButUnaddedLicensePlate: SCLicensePlate = 'a-fh';
    expect(isValidSCNamespace(existingAddedSCLIcensePlate)).to.be.equal(true);
    expect(isValidSCNamespace(notASCLicensePlate)).to.be.equal(false);
    expect(isValidSCNamespace(existingButUnaddedLicensePlate)).to.be.equal(false);
  }

  @test
  testCreateUUID() {
    const item = {
      type: 'Not even a thing'
    };
    expect(createUUID(item,'f-u')).to.equal('3ac2b548-75d3-5326-920a-241e514fe445');
    // ID was generated once before!
  }

  @test
  async testExecuteConnector() {
    const source = CommonSpec.connector.origin;

    const bulkOpen: SCBulkResponse = {
      source: source,
      state: 'in progress',
      type: SCThingType.Message,
      uid: '744321ca-cc95-4967-b8df-42c98b792db6',
    };

    nock('http://localhost:3000')
    .post('/bulk')
    .reply(200, bulkOpen);

    const nockBulkAdd = nock('http://localhost:3000')
    .post('/bulk/744321ca-cc95-4967-b8df-42c98b792db6')
    .reply(201, {})
    .persist(); // otherwise consumed!

    nock('http://localhost:3000')
    .post('/bulk/744321ca-cc95-4967-b8df-42c98b792db6/done')
    .reply(204, {});

    // should succeed
    expect(await CommonSpec.runExecuteConnector()).to.equal(true);

    nockBulkAdd.persist(false);

    // should fail due to nockBulkAdd being consumed
    expect(await CommonSpec.runExecuteConnector()).to.equal(false);

  }

  /**
   * Executes the connector
   */
  static async runExecuteConnector(): Promise<boolean> {
    try{
      await executeConnector('http://localhost:3000', CommonSpec.connector)
      return true;
    }catch(err){
      return false;
    }
  }
}
