# [0.2.0](https://gitlab.com/openstapps/minimal-connector/compare/v0.1.0...v0.2.0) (2019-02-07)



# [0.1.0](https://gitlab.com/openstapps/minimal-connector/compare/v0.0.2...v0.1.0) (2019-01-30)



## [0.0.2](https://gitlab.com/openstapps/minimal-connector/compare/v0.0.1...v0.0.2) (2018-12-03)



## [0.0.1](https://gitlab.com/openstapps/minimal-connector/compare/d332f6e...v0.0.1) (2018-12-03)


### Features

* add minimal connector ([d332f6e](https://gitlab.com/openstapps/minimal-connector/commit/d332f6e))



