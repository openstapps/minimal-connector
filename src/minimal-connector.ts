/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCLicensePlate, SCMessage, SCThingRemoteOrigin, SCThingType} from '@openstapps/core';
import {createUUID} from './common';
import {Connector} from './connector';

/**
 * Example connector
 */
export class MinimalConnector extends Connector<SCMessage> {
  /**
   * Type of the things handled in the connector for quick access to the type
   */
  private readonly type: SCThingType.Message = SCThingType.Message;

  /**
   * Constructor for the MinimalConnector
   * 
   * @param licensePlate License plate of the school
   * @param origin Name of the connector
   */
  constructor(licensePlate: SCLicensePlate, origin: string) {
    super(licensePlate, origin);
  }

  /**
   * Use or override the `createRemoteOrigin` method to customize the remote origin
   */
  private createCustomRemoteOrigin(): SCThingRemoteOrigin {
    const customRemoteOrigin = this.createRemoteOrigin();
    // may add a maintainer or other attributes here
    customRemoteOrigin.url = 'http://your.backend.url';

    return customRemoteOrigin;
  }

  /**
   * Mock-up data
   */
  protected async fetchItems(): Promise<SCMessage[]> {
    const importedItems: SCMessage[] = [
      {
        audiences: ['students', 'employees'],
        description: 'Some description 1',
        messageBody: 'Some message 1',
        name: 'Some name 1',
        origin: this.createCustomRemoteOrigin(),
        type: this.type,
        uid: createUUID({id: 'message_1'}, this.licensePlate),
      },
      {
        audiences: ['students', 'employees'],
        description: 'Some description 2',
        messageBody: 'Some message 2',
        name: 'Some name 2',
        origin: this.createCustomRemoteOrigin(),
        type: this.type,
        uid: '', // see Connetor.getItems()
      },
      {
        audiences: ['students', 'employees'],
        description: 'Some description 3',
        messageBody: 'Some message 3',
        name: 'Some name 3',
        origin: this.createCustomRemoteOrigin(),
        type: this.type,
        uid: createUUID({id: 'message_3'}, this.licensePlate),
      },
    ];

    return importedItems;
  }
}
