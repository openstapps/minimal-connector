/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {Command} from 'commander';
import {readFileSync} from 'fs';
import {join} from 'path';
import {executeConnector, isValidSCNamespace} from './common';
import {MinimalConnector} from './minimal-connector';

process.on('unhandledRejection', (error) => {
  throw error;
});

const connectorVersion = JSON.parse(
  readFileSync(join(__dirname, '..', 'package.json'))
    .toString(),
).version;

const commander = new Command();

/**
 * Uses arguments to paramtrize the connector execution
 * 
 * backendURL - URL of the StApps backend deployment e.g. http://localhost:3000
 * origin - Origin, where the data comes from. Typically the name of the connector e.g. minimal-connector
 * licensePlate - The license plate of your school. Must be matched to a SCNamespace e.g. f-u
 */
commander
  .command('run <backendURL> <origin> <licensePlate>')
  .version(connectorVersion)
  .action(async (backendURL: string, origin: string, licensePlate: string) => {

      if (backendURL.length === 0) {
        throw new Error('Param "backend" needs to have a length greater zero.');
      }
      const originRegex = /^[a-z\-\_0-9]+$/;
      if (!originRegex.test(origin)) {
        throw new Error('Origin name can only consist of lowercase letters from a-z, "-", "_" and integer numbers.');
      }

      if (!isValidSCNamespace(licensePlate)) {
        throw new Error('Not a valid license plate. Please register a namespace with a unique-id in "core"');
      }

      // TODO for connector-developers: set your connector here
      const connector = new MinimalConnector(licensePlate, origin);

      await executeConnector(backendURL, connector);
      Logger.ok('Done');
    },
  );

commander.parse(process.argv);
