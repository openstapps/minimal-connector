# @openstapps/minimal-connector

## Prerequisites:

* `node` (version 10 LTS) and `npm` installed

* a backend, which is running locally (on http://localhost:3000) – for testing purposes, it is advisable to use `minimal-deployment` project

## How to get started

To install all needed `npm` packages:
```shell
npm install
```

To prepare the script to be executed, run:
```shell
npm run build
```

To execute the code in "CLI" script (basically to execute the connector):
```shell
node lib/cli.js run <backendURL> <origin> <licensePlate>
e.g.:
node lib/cli.js run http://localhost:3000 minimal-connector f-u
```

To run some sample tests, use:
```shell
npm test
```

The `npm` scripts are defined in `package.json` file.

After building the project once, the connector image can be build.
```shell
docker build .
```

Use the following command to run the container of this connector. Assuming there is a local backend running.
```shell
docker run --network host <image-id> <backendURL> <origin> <licensePlate>
e.g.:
docker run --network host registry.gitlab.com/openstapps/minimal-connector http://localhost:3000 minimal-connector f-u
```

## Creating your own connector

1. Update the `executeConnector`-function in `cli.ts` according to the comments and needs of your connector
2. Implement the `fetchItems()` function in `MinimalConnector.ts`
3. Test your connector in your test environment by passing the according arguments to the CLI (See execution-example above)

## Next steps

You may want to:
* modify the convenience methods in the `minimal-connector/api` to your needs.
* add additional options to your cli.

Explore open source connectors from other schools to get some ideas.

## Go into production

1. Deploy your connector.
2. Adjust the CLI-args to fit your production mode

## Code structure

Folder [src](src/) contains:
* Reference implementations for CLI and a connector, using the api-classes.
* [/cli.ts](src/cli.ts) 
  * minimal CLI to start your connector, that uses CLI-args, so that there are no hard coded values for configuration.
  * will execute the specified connectors and push the data to the backend
* [/common.ts](src/api/Connector.ts)
  * `createUUID`, that will generate a unique id for a given identifying object
  * `executeConnector`, that will execute the connector, which will fetch the items and push them to the backend 
* [/Connector.ts](src/api/Connector.ts) abstracts the process of executing your specific connector and creating unique ids for the imported items
* [/MinimalConnector.ts](src/MinimalConnector.ts) example connector with mock-up data
  * shows how to instantiate things
  * shows how to use the convenience functions

File [test/MinimalConnector.spec.ts](test/MinimalConnector.spec.ts) contains sample test suite using `mocha` and `chai`.
